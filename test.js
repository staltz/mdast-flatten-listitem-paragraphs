const test = require('tape');
const unified = require('unified');
const remarkParse = require('remark-parse');
const inspect = require('unist-util-inspect');
const flattenListItemParagraphs = require('./index');

test('it converts "listItem > paragraph" to "listItem"', t => {
  t.plan(2);

  const markdown = `
# Title

- First
- The **second** item
`;

  const actualInput = unified()
    .use(remarkParse, {commonmark: true})
    .parse(markdown);

  // console.log(JSON.stringify(actualInput));
  console.log(inspect(actualInput));

  const expectedInput = {
    type: 'root',
    children: [
      {
        type: 'heading',
        depth: 1,
        children: [
          {
            type: 'text',
            value: 'Title',
            position: {
              start: {line: 2, column: 3, offset: 3},
              end: {line: 2, column: 8, offset: 8},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 2, column: 1, offset: 1},
          end: {line: 2, column: 8, offset: 8},
          indent: [],
        },
      },
      {
        type: 'list',
        ordered: false,
        start: null,
        loose: false,
        children: [
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [
              {
                type: 'paragraph',
                children: [
                  {
                    type: 'text',
                    value: 'First',
                    position: {
                      start: {line: 4, column: 3, offset: 12},
                      end: {line: 4, column: 8, offset: 17},
                      indent: [],
                    },
                  },
                ],
                position: {
                  start: {line: 4, column: 3, offset: 12},
                  end: {line: 4, column: 8, offset: 17},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 4, column: 1, offset: 10},
              end: {line: 4, column: 8, offset: 17},
              indent: [],
            },
          },
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [
              {
                type: 'paragraph',
                children: [
                  {
                    type: 'text',
                    value: 'The ',
                    position: {
                      start: {line: 5, column: 3, offset: 20},
                      end: {line: 5, column: 7, offset: 24},
                      indent: [],
                    },
                  },
                  {
                    type: 'strong',
                    children: [
                      {
                        type: 'text',
                        value: 'second',
                        position: {
                          start: {line: 5, column: 9, offset: 26},
                          end: {line: 5, column: 15, offset: 32},
                          indent: [],
                        },
                      },
                    ],
                    position: {
                      start: {line: 5, column: 7, offset: 24},
                      end: {line: 5, column: 17, offset: 34},
                      indent: [],
                    },
                  },
                  {
                    type: 'text',
                    value: ' item',
                    position: {
                      start: {line: 5, column: 17, offset: 34},
                      end: {line: 5, column: 22, offset: 39},
                      indent: [],
                    },
                  },
                ],
                position: {
                  start: {line: 5, column: 3, offset: 20},
                  end: {line: 5, column: 22, offset: 39},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 5, column: 1, offset: 18},
              end: {line: 5, column: 22, offset: 39},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 4, column: 1, offset: 10},
          end: {line: 5, column: 22, offset: 39},
          indent: [1],
        },
      },
    ],
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 6, column: 1, offset: 40},
    },
  };

  t.deepEquals(actualInput, expectedInput, 'input looks good');

  const actualOutput = flattenListItemParagraphs()(actualInput);

  // console.log(JSON.stringify(actualOutput));
  console.log(inspect(actualOutput));

  const expectedOutput = {
    type: 'root',
    children: [
      {
        type: 'heading',
        depth: 1,
        children: [
          {
            type: 'text',
            value: 'Title',
            position: {
              start: {line: 2, column: 3, offset: 3},
              end: {line: 2, column: 8, offset: 8},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 2, column: 1, offset: 1},
          end: {line: 2, column: 8, offset: 8},
          indent: [],
        },
      },
      {
        type: 'list',
        ordered: false,
        start: null,
        loose: false,
        children: [
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [
              {
                type: 'text',
                value: 'First',
                position: {
                  start: {line: 4, column: 3, offset: 12},
                  end: {line: 4, column: 8, offset: 17},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 4, column: 1, offset: 10},
              end: {line: 4, column: 8, offset: 17},
              indent: [],
            },
          },
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [
              {
                type: 'text',
                value: 'The ',
                position: {
                  start: {line: 5, column: 3, offset: 20},
                  end: {line: 5, column: 7, offset: 24},
                  indent: [],
                },
              },
              {
                type: 'strong',
                children: [
                  {
                    type: 'text',
                    value: 'second',
                    position: {
                      start: {line: 5, column: 9, offset: 26},
                      end: {line: 5, column: 15, offset: 32},
                      indent: [],
                    },
                  },
                ],
                position: {
                  start: {line: 5, column: 7, offset: 24},
                  end: {line: 5, column: 17, offset: 34},
                  indent: [],
                },
              },
              {
                type: 'text',
                value: ' item',
                position: {
                  start: {line: 5, column: 17, offset: 34},
                  end: {line: 5, column: 22, offset: 39},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 5, column: 1, offset: 18},
              end: {line: 5, column: 22, offset: 39},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 4, column: 1, offset: 10},
          end: {line: 5, column: 22, offset: 39},
          indent: [1],
        },
      },
    ],
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 6, column: 1, offset: 40},
    },
  };

  t.deepEquals(actualOutput, expectedOutput, 'output looks good');
});
