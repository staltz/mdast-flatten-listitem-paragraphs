# mdast-flatten-listitem-paragraphs

Transforms a markdown AST ([MDAST](https://github.com/syntax-tree/mdast)) so that listItem nodes do not contain a paragraph node as child. Mutates the input AST.

## Installation

```sh
npm install mdast-flatten-listitem-paragraphs
```

## Usage

```js
// ...
var flattenListItemParagraphs = require('mdast-flatten-listitem-paragraphs');

unified()
  .use(parse)
  .use(flattenListItemParagraphs)
// ...
```

Markdown document:

```
# Title

- First
- The **second** item
```

Input AST:

```
root[2] (1:1-6:1, 0-40)
├─ heading[1] (2:1-2:8, 1-8) [depth=1]
│  └─ text: "Title" (2:3-2:8, 3-8)
└─ list[2] (4:1-5:22, 10-39) [ordered=false][loose=false]
   ├─ listItem[1] (4:1-4:8, 10-17) [loose=false]
   │  └─ paragraph[1] (4:3-4:8, 12-17)
   │     └─ text: "First" (4:3-4:8, 12-17)
   └─ listItem[1] (5:1-5:22, 18-39) [loose=false]
      └─ paragraph[3] (5:3-5:22, 20-39)
         ├─ text: "The " (5:3-5:7, 20-24)
         ├─ strong[1] (5:7-5:17, 24-34)
         │  └─ text: "second" (5:9-5:15, 26-32)
         └─ text: " item" (5:17-5:22, 34-39)
```

Output AST:

```
root[2] (1:1-6:1, 0-40)
├─ heading[1] (2:1-2:8, 1-8) [depth=1]
│  └─ text: "Title" (2:3-2:8, 3-8)
└─ list[2] (4:1-5:22, 10-39) [ordered=false][loose=false]
   ├─ listItem[1] (4:1-4:8, 10-17) [loose=false]
   │  └─ text: "First" (4:3-4:8, 12-17)
   └─ listItem[3] (5:1-5:22, 18-39) [loose=false]
      ├─ text: "The " (5:3-5:7, 20-24)
      ├─ strong[1] (5:7-5:17, 24-34)
      │  └─ text: "second" (5:9-5:15, 26-32)
      └─ text: " item" (5:17-5:22, 34-39)
```

## License

[MIT](LICENSE)